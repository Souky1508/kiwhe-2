
 // permet de lancer le JS uniquement après que la page HTML soit chargée. Mettre tout le JS dans les {} de document.addEventListener
document.addEventListener('DOMContentLoaded', function() {   
    
    var lettreDpe;
    var energie;
    var surface;
    var conso_min;
    var conso_max;
    var cout_min;
    var cout_max;

    function calculDpe(){
        
        //valeur DPE
        lettreDpe = false;
        var listeDpe = document.getElementsByName('classe'); // on récupère le tableau "Classe"
        var i;
        for (i = 0; i < listeDpe.length; i++) {      
            if (listeDpe[i].checked == true) {       
                lettreDpe = listeDpe[i].value;          
            }

        };           

        //valeur Energie
        energie = false;
        var listeEnergie = document.getElementsByName('energie'); // on récupère le tableau "Energie"
        var i;
        for (i = 0; i < listeEnergie.length; i++) {
            if (listeEnergie[i].checked == true) {
                energie = listeEnergie[i].value;
            }
        };

        //valeur surface
        surface = false;
        surface = document.getElementsByName('surface')[0].value;

        //switch du choix de la classe DPE
        switch (lettreDpe) {
            case "A":
                min = 1;
                max = 50;
                break;
            case "B":
                min = 51;
                max = 90;
                break;
            case "C":
                min = 91
                max = 150;
                break;
            case "D":
                min = 151;
                max = 230;
                break;
            case "E":
                min = 231;
                max = 330;
                break;
            case "F":
                min = 331;
                max = 450;
                break;
            case "G":
                min = 451;
                max = 1000;
                break;
        };
              
        //switch du cout de l'énergie
        switch (energie) {
            case "electricite" :
                cout_energie = 0.152;
                break;
            case "gaz_p" :
                cout_energie = 0.163;
                break;
            case "gaz_n":
                cout_energie = 0.086;
                break;
            case "fioul":
                cout_energie = 0.101;
                break;
            case "bois_b":
                cout_energie = 0.043;
                break;
            case "bois_g" :
                cout_energie = 0.073;
                break;
        };

        //calcul conso min & max
        conso_min = min * surface;
        conso_max = max * surface;
    
        //calcul min & max
        cout_min = conso_min * cout_energie;
        cout_max = conso_max * cout_energie;

        //affiche les résultats dans la page html
        document.getElementById('budgetMin').innerHTML = cout_min.toFixed(2);
        document.getElementById('budgetMax').innerHTML = cout_max.toFixed(2);
        document.getElementById('consoMin').innerHTML = conso_min;
        document.getElementById('consoMax').innerHTML = conso_max;   
    };

        //on affiche les résultats obtenu au changement d'état de la page
        document.addEventListener("change", function() {
            calculDpe();
            creaUrl();
        }, false );


        // Pour la création du PDF
        function creaUrl()
        {
            var ma_lettreDpeUrl = lettreDpe;
            var ma_surfaceUrl = surface;    
            var mon_energieUrl = energie;   
            var mon_cout_min = cout_min.toFixed(2);    
            var mon_cout_max = cout_max.toFixed(2);    
            var ma_conso_min = conso_min;   
            var ma_conso_max = conso_max;   
        
            var urlKiwhe = "/Kiwhe.html?classe="+ma_lettreDpeUrl+"&surface="+ma_surfaceUrl+"&energie="+mon_energieUrl+"&cout_min="+mon_cout_min+"&cout_max="+mon_cout_max+"&ma_conso_min="+ma_conso_min+"&ma_conso_max="+ma_conso_max;
            var urlKiwhe = escape(urlKiwhe);
            var url = "http://api.pdflayer.com/api/convert?access_key=49f2e7d94e76b21116feb3d72745cd6b&document_url="+urlKiwhe+"&inline=0&test=1&document_name=kiwhe.pdf"
        
            document.getElementById("pdf").innerHTML = 
            '<a href="'+url+'"><img src="image/exporter.png" alt="exporter"/></a>' 
        }

}, false );

        //afficher une image de check quand une valeur est saisie
        //pour le DPE
        function afficheCheckDpe() {
            document.getElementById('checkDpe').innerHTML= "<img src=\"image/check.png\">";   
        };
        //pour la surface
        function afficheCheckSurface() {
            if ((document.getElementsByName('surface')[0].value<=0) || (document.getElementsByName('surface')[0].value > 1000)){
                document.getElementById('checkSurface').innerHTML="";
            }
            else {
                document.getElementById('checkSurface').innerHTML= "<img src=\"image/check.png\">";
            }  
        };
        //pour l'Energie
        function afficheCheckEnergie() {
            document.getElementById('checkEnergie').innerHTML= "<img src=\"image/check.png\">";   
        };
