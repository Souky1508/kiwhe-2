// Je crée un objet $_GET qui utilise URLSearchParams
window.$_GET = new URLSearchParams(location.search);

//Je passes les variables GET dans des variables JS
var surfacePdf = $_GET.get('ma_surfaceUrl');
var lettreDpePdf = $_GET.get('ma_lettreDpeUrl');
var energiePdf = $_GET.get('mon_energieUrl');
var cout_minPdf = $_GET.get('mon_cout_min');
var cout_maxPdf = $_GET.get('mon_cout_max');
var conso_minPdf = $_GET.get('ma_conso_min');
var conso_maxPdf = $_GET.get('ma_conso_max');



// On affiche les variables dans la page html_pdf
    // Je récupère le bon picto de la Classe et je l'intègre
    switch (lettreDpePdf) {
        case "A":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Abis.png\" />";
            break;
        case "B":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Bbis.png\" />";
            break;
        case "C":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Cbis.png\" />";
            break;
        case "D":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Dbis.png\" />";
            break;
        case "E":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Ebis.png\" />";
            break;
        case "F":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Fbis.png\" />";    
            break;
        case "G":
        document.getElementById('picto_dpe').innerHTML="<img src=\"image/Gbis.png\" />";
            break;
    };

    // Je récupère le bon picto de l'Energie et je l'intègre
    switch (energiePdf) {
        case "electricite" :
        document.getElementById('picto_energie').innerHTML="<img src=\"image/elecBIS.png\" />";    
            break;
        case "gaz_p" :
        document.getElementById('picto_energie').innerHTML="<img src=\"image/Gaz_PropaneBIS.png\" />";    
            break;
        case "gaz_n":
        document.getElementById('picto_energie').innerHTML="<img src=\"image/Gaz_NaturelBIS.png\" />";    
            break;
        case "fioul":
        document.getElementById('picto_energie').innerHTML="<img src=\"image/FioulBIS.png\" />";   
            break;
        case "bois_b":
        document.getElementById('picto_energie').innerHTML="<img src=\"image/BoisBIS.png\" />";    
            break;
        case "bois_g" :
        document.getElementById('picto_energie').innerHTML="<img src=\"image/GranuléBIS.png\" />";    
            break;
    };

    // J'intègre la valeur de la surface à l'input
    document.getElementById('surface').value= surfacePdf;

    //j'intègre la conso Min et Max en Euros
    document.getElementById('budgetMin').innerHTML= cout_minPdf;
    document.getElementById('budgetMax').innerHTML= cout_maxPdf;

    //j'intègre la conso Min et Max en kWh
    document.getElementById('consoMin').innerHTML= conso_minPdf;
    document.getElementById('consoMax').innerHTML= conso_maxPdf;