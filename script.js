// picto indice

$('.CA').on({
  'click': function(){
  $('.CA').attr('src','image/Abis.png');
  $('.CB').attr('src','image/B.png');
  $('.CC').attr('src','image/C.png');
  $('.CD').attr('src','image/D.png');
  $('.CE').attr('src','image/E.png');
  $('.CF').attr('src','image/F.png');
  $('.CG').attr('src','image/G.png');
  }
});


$('.CB').on({
  'click': function(){
  $('.CB').attr('src','image/Bbis.png');
  $('.CA').attr('src','image/A.png');
  $('.CC').attr('src','image/C.png');
  $('.CD').attr('src','image/D.png');
  $('.CE').attr('src','image/E.png');
  $('.CF').attr('src','image/F.png');
  $('.CG').attr('src','image/G.png');
  }
});


$('.CC').on({
  'click': function(){
  $('.CC').attr('src','image/Cbis.png');
  $('.CB').attr('src','image/B.png');
  $('.CA').attr('src','image/A.png');
  $('.CD').attr('src','image/D.png');
  $('.CE').attr('src','image/E.png');
  $('.CF').attr('src','image/F.png');
  $('.CG').attr('src','image/G.png');
  }
});

$('.CD').on({
  'click': function(){
  $('.CD').attr('src','image/Dbis.png');
  $('.CB').attr('src','image/B.png');
  $('.CC').attr('src','image/C.png');
  $('.CA').attr('src','image/A.png');
  $('.CE').attr('src','image/E.png');
  $('.CF').attr('src','image/F.png');
  $('.CG').attr('src','image/G.png');
  }
});

$('.CE').on({
  'click': function(){
  $('.CE').attr('src','image/Ebis.png');
  $('.CB').attr('src','image/B.png');
  $('.CC').attr('src','image/C.png');
  $('.CD').attr('src','image/D.png');
  $('.CA').attr('src','image/A.png');
  $('.CF').attr('src','image/F.png');
  $('.CG').attr('src','image/G.png');
  }
});

$('.CF').on({
  'click': function(){
  $('.CF').attr('src','image/Fbis.png');
  $('.CB').attr('src','image/B.png');
  $('.CC').attr('src','image/C.png');
  $('.CD').attr('src','image/D.png');
  $('.CE').attr('src','image/E.png');
  $('.CA').attr('src','image/A.png');
  $('.CG').attr('src','image/G.png');
  }
});

$('.CG').on({
  'click': function(){
    $('.CG').attr('src','image/Gbis.png');
  
    $('.CB').attr('src','image/B.png');
    $('.CC').attr('src','image/C.png');
    $('.CD').attr('src','image/D.png');
    $('.CE').attr('src','image/E.png');
    $('.CF').attr('src','image/F.png');
    $('.CA').attr('src','image/A.png');
  }
});

// picto chauffage

$('.elec').on({
  'click': function(){
  $('.elec').attr('src','image/elecbis.png');
  $('.gazn').attr('src','image/Gaz_Naturel.png');
  $('.bois').attr('src','image/Bois.png');
  $('.fioul').attr('src','image/Fioul.png');
  $('.gazp').attr('src','image/Gaz_Propane.png');
  $('.granule').attr('src','image/Granulé.png');
  }
});

$('.gazn').on({
  'click': function(){
  $('.gazn').attr('src','image/Gaz_NaturelBIS.png');
  $('.bois').attr('src','image/Bois.png');
  $('.fioul').attr('src','image/Fioul.png');
  $('.gazp').attr('src','image/Gaz_Propane.png');
  $('.granule').attr('src','image/Granulé.png');
  $('.elec').attr('src','image/elec.png');
  }
});

$('.bois').on({
  'click': function(){
  $('.bois').attr('src','image/BoisBIS.png');
  $('.fioul').attr('src','image/Fioul.png');
  $('.gazp').attr('src','image/gaz_propane.png');
  $('.granule').attr('src','image/Granulé.png');
  $('.elec').attr('src','image/elec.png');
  $('.gazn').attr('src','image/gaz_naturel.png');
  }
});

$('.fioul').on({
  'click': function(){
  $('.fioul').attr('src','image/FioulBIS.png');
  $('.gazp').attr('src','image/Gaz_Propane.png');
  $('.granule').attr('src','image/Granulé.png');
  $('.elec').attr('src','image/elec.png');
  $('.gazn').attr('src','image/Gaz_Naturel.png');
  $('.bois').attr('src','image/Bois.png');
  }
});

$('.gazp').on({
  'click': function(){
  $('.gazp').attr('src','image/Gaz_propaneBIS.png');
  $('.bois').attr('src','image/Bois.png');
  $('.fioul').attr('src','image/Fioul.png');
  $('.granule').attr('src','image/Granulé.png');
  $('.elec').attr('src','image/elec.png');
  $('.gazn').attr('src','image/Gaz_Naturel.png');
  }
});

$('.granule').on({
  'click': function(){
  $('.granule').attr('src','image/GranuléBIS.png');
  $('.fioul').attr('src','image/Fioul.png');
  $('.gazp').attr('src','image/Gaz_Propane.png');
  $('.elec').attr('src','image/elec.png');
  $('.gazn').attr('src','image/Gaz_Naturel.png');
  $('.bois').attr('src','image/Bois.png');
  }
});

// réinitialiser

 $('.refaire').click(function(){

  $('.formulaire')[0].reset();

  $('.CG').attr('src','image/G.png');
  $('.CB').attr('src','image/B.png');
  $('.CC').attr('src','image/C.png');
  $('.CD').attr('src','image/D.png');
  $('.CE').attr('src','image/E.png');
  $('.CF').attr('src','image/F.png');
  $('.CA').attr('src','image/A.png');
  
  $('.granule').attr('src','image/Granulé.png');
  $('.fioul').attr('src','image/Fioul.png');
  $('.gazp').attr('src','image/Gaz_Propane.png');
  $('.elec').attr('src','image/elec.png');
  $('.gazn').attr('src','image/Gaz_Naturel.png');
  $('.bois').attr('src','image/Bois.png');

  $('#checkEnergie').attr("");

  document.getElementById('budgetMin').innerHTML = "";
  document.getElementById('budgetMax').innerHTML = "";
  document.getElementById('consoMin').innerHTML = "";
  document.getElementById('consoMax').innerHTML = "";  

  document.getElementById('checkDpe').innerHTML= ""; 
  document.getElementById('checkSurface').innerHTML= "";
  document.getElementById('checkEnergie').innerHTML= "";
})


