
 // permet de lancer le JS uniquement après que la page HTML soit chargée. Mettre tout le JS dans les {} de document.addEventListener
 document.addEventListener('DOMContentLoaded', function() {   
    

    function calculDpe(){
        //recupere les données : dpe, surface, chauffage
        
        //valeur DPE
        var lettreDpe = false;
        var listeDpe = document.getElementsByName('classe'); // on récupère le tableau "Classe"
        var i;
        for (i = 0; i < listeDpe.length; i++) {      // Parcours du tableau avec la boucle
            if (listeDpe[i].checked == true) {                 // Vérif si la case est cochée
                lettreDpe = listeDpe[i].value;           // si case cochée, on donne à la Var la valeur de la case cochée
            }
        };           

        //valeur Energie
        var energie = false;
        var listeEnergie = document.getElementsByName('energie'); // on récupère le tableau "Energie"
        var i;
        for (i = 0; i < listeEnergie.length; i++) {
            if (listeEnergie[i].checked == true) {
                energie = listeEnergie[i].value;
            }
        };
        //valeur surface
        var surface = false;
        surface = document.getElementsByName('surface')[0].value;
         

        //On vérifie toute les données récupérées 
        // function verif(surface, energie, lettreDpe) {
        //     var erreurs = "";
        //     if (isNaN(surface)) {
        //         erreurs += "Veuillez rentrer un chiffre !";
        //     }
        //     else if ((surface<=0) || (surface > 1000)){
        //         erreurs += "Veuillez saisir une surface réellement habitable !\n";
            
        //     }
        //     if (energie == false) {
        //         erreurs += "Veuillez choisir un type d'énergie !\n";
                
        //     }
        //     if (lettreDpe == false) {
        //         erreurs += "Veuillez choisir un indice énergétique !\n";
        //     } 
        //     if(erreurs!=""){
        //         alert(erreurs);
        //         return false;
        //     }else{
        //         return true;
        //     }
        // };
     
    
        //switch du choix de la classe DPE
        switch (lettreDpe) {
            case "A":
                min = 1;
                max = 50;
                break;
            case "B":
                min = 51;
                max = 90;
                break;
            case "C":
                min = 91
                max = 150;
                break;
            case "D":
                min = 151;
                max = 230;
                break;
            case "E":
                min = 231;
                max = 330;
                break;
            case "F":
                min = 331;
                max = 450;
                break;
            case "G":
                min = 451;
                max = 1000;
                break;
        };
        
        
        //switch du cout de l'énergie
        switch (energie) {
            case "electricite" :
                cout_energie = 0.152;
                break;
            case "gaz_p" :
                cout_energie = 0.163;
                break;
            case "gaz_n":
                cout_energie = 0.086;
                break;
            case "fioul":
                cout_energie = 0.101;
                break;
            case "bois_b":
                cout_energie = 0.043;
                break;
            case "bois_g" :
                cout_energie = 0.073;
                break;
        };

    
        //calcul conso min & max
        var conso_min = min * surface;
        var conso_max = max * surface;
    
        //calcul min & max
        var cout_min = conso_min * cout_energie;
        var cout_max = conso_max * cout_energie;

        //affiche les résultats dans la page html
        // var resultat = document.getElementById('conso');
        // var resultatBis = document.getElementById('budget');
        document.getElementById('consoMin').innerHTML = conso_min;
        document.getElementById('consoMax').innerHTML = conso_max;
        document.getElementById('budgetMin').innerHTML = cout_min.toFixed(2);
        document.getElementById('budgetMax').innerHTML = cout_max.toFixed(2);
    };



        //on affiche les résultats obtenu au changement d'état de la page
        document.addEventListener("change", function() {
            calculDpe();
        }, false );

        }, false);

    //afficher une image de check quand une valeur est saisie
        //pour le DPE
        function afficheCheckDpe() {
            document.getElementById('checkDpe').innerHTML= "<img src=\"image/check.png\">";   
        };
        //pour la surface
        function afficheCheckSurface() {
            document.getElementById('checkSurface').innerHTML= "<img src=\"image/check.png\">";  
        };
        //pour l'Energie
        function afficheCheckEnergie() {
            document.getElementById('checkEnergie').innerHTML= "<img src=\"image/check.png\">";   
        };

        // mettre une condition pour afficher les Checks 

        // mettre une condition pour effacer les Checks

        // afficher un message d'erreur permanent tant que les  checks ne sont pas affichés ensemble

        //affiche le résultat quand les 3 checks sont affichés
  
    
        function verifDpe() { 
            if (lettreDpe == false) {
                document.getElementById('info').innerHTML = "Veuillez entrer toutes les données pour obtenir votre résultat !\n";
            }   
        }

        function verifSurface() {
            var erreurs = document.getElementById('info');
            if (isNaN(surface)) {
                erreurs.innerHTML += "Veuillez entrer toutes les données pour obtenir votre résultat !\n";
            }
            else if ((surface<=0) || (surface > 1000)){
                erreurs.innerHTML += "Veuillez entrer toutes les données pour obtenir votre résultat !\n";
            }
        }

        function verifEnergie() {
            var erreurs = document.getElementById('info');
            if (energie == false) {
                erreurs.innerHTML += "Veuillez entrer toutes les données pour obtenir votre résultat !\n";    
            }
        }